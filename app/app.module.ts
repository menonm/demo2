﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend

import { AppComponent }  from './app.component';

import { HeaderComponent } from './header/header.component';
import { LeftHeaderComponent } from './leftheader/leftheader.component';
import { DropdownComponent } from './dropdown/dropdown.component';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule
      
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
    LeftHeaderComponent,
    DropdownComponent
       
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }