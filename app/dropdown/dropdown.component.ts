import { Component, Output, EventEmitter, Input } from '@angular/core';




@Component({
    moduleId: module.id,
    selector: 'dropdown',
    templateUrl: 'dropdown.component.html'
})

export class DropdownComponent {
    @Output() toggleDropDown: EventEmitter<any> = new EventEmitter();
    @Input() selectedText: any;
    @Input() inputValues: any;
    @Input() dropdownStyle: string;
    @Output() itemsDropDownEvent: EventEmitter<any> = new EventEmitter();

    constructor(
    ) { }


    /**
* function for toggling of dropdown
* @method toggleDropDowns
* @param {event} event
*/
    toggleDropDowns(event: Event) {
        this.toggleDropDown.emit({
            value: event
        })

    }


    /**
* function for setting up the values and changing the menu
* @method itemDropDownEvent
* @param {event} event
*/
    itemDropDownEvent(event: Event) {
        this.itemsDropDownEvent.emit({
            value: event
        })

    }


}