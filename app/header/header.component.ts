import { Component } from '@angular/core'

declare var jQuery: any
@Component({
    moduleId: module.id,
    selector: 'header',
    templateUrl: 'header.component.html'
})

export class HeaderComponent {
    model: any = {};
    loading = false;
    returnUrl: string;
    values = ['student@school.org', 'Settings', 'Sign Out'];
    selected_text = 'teacher@school.org';


    constructor(
    ) { }



    /**
* function for toggling of dropdown
* @method toggleDropDown
* @param {event} event
*/
    toggleDropDown(event: Event) {
        console.log(jQuery(Object(event).value))
        if (jQuery(Object(event).value.currentTarget).hasClass("drop-downclicked") == false) {
            jQuery(Object(event).value.currentTarget).addClass("drop-downclicked");
            jQuery(".filter-text").addClass("drop-downclicked");
            jQuery(".content-filter-dDown-content").removeClass("display-none");
        } else {
            jQuery(Object(event).value.currentTarget).removeClass("drop-downclicked");
            jQuery(".filter-text").removeClass("drop-downclicked");
            jQuery(".content-filter-dDown-content").addClass("display-none");
        }
    }


    /**
* function for setting up the values and changing the menu
* @method itemsDropDownEvent
* @param {event} event
*/
    itemsDropDownEvent(event: Event) {
        if (jQuery(Object(event).value.target).attr("id").indexOf("student") >= 0) {
            this.showItem("student-label");
            this.hideItem("teacher-label");
        } else if (jQuery(Object(event).value.target).attr("id").indexOf("teacher") >= 0) {
            this.showItem("teacher-label");
            this.hideItem("student-label");
        }
        if (jQuery(Object(event).value.target).attr("id").indexOf("student") >= 0 || jQuery(Object(event).value.target).attr("id").indexOf("teacher") >= 0) {
            var selected_item = this.selected_text;
            this.values.splice(0, 1)
            this.values.unshift(selected_item);
            this.selected_text = jQuery(Object(event).value.target).attr("id");
        }
        jQuery(".filter-drps").trigger("click");


    }


    /**
* common function to show item
* @method showItem
* @param {item} name of the item
*/
    showItem(item: string) {
        jQuery("." + item).removeClass("display-none")
    }

    /**
* common function to hide item
* @method hideItem
* @param {item} name of the item
*/

    hideItem(item: string) {
        jQuery("." + item).addClass("display-none")
    }
}
